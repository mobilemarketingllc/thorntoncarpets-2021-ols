<?php get_header(); ?>

    <div class="fl-archive container ">
        <div class="row search-row">

            <?php FLTheme::sidebar('left'); ?>

            <div class="fl-content fl-builder-content  <?php //FLTheme::content_class(); ?>" itemscope="itemscope" itemtype="http://schema.org/SearchResultsPage">

                <?php FLTheme::archive_page_header(); 
                 $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                $s=get_search_query();
                $args = array(
                                's' =>$s,
                                'paged' => $paged,
                                'meta_key' => 'swatch_image_link',
                                'meta_value' => '',
                                'meta_compare' => '!='
                            );
                // The Query
                $the_query = new WP_Query( $args );
                ?>

                <?php if($the_query->have_posts()) : ?>

                    <?php
                    include( ABSPATH .'/wp-content/plugins/grand-child/product-listing-templates/product-loop-search.php' );
                    ?>

                    <?php FLChildTheme::archive_nav(); ?>

                <?php else : ?>

                    <?php get_template_part('content', 'no-results'); ?>

                <?php endif; ?>

            </div>



            <?php //FLTheme::sidebar('right'); ?>

        </div>
    </div>

<?php get_footer(); ?>