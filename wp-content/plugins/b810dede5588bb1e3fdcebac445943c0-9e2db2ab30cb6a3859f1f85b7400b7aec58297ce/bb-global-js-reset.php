<?php
/*
Plugin Name: Beaver Builder Global JS Reset
Description: Resets global JS and self deactivates.
Author: <Simon>
Version: 1.0
*/
class BB_Global_JS_Reset {
	function __construct() {
		include_once ABSPATH . '/wp-admin/includes/plugin.php';
		$settings     = get_option( '_fl_builder_settings' );
		$settings->js = '';
		update_option( '_fl_builder_settings', $settings );
		deactivate_plugins( plugin_basename( __FILE__ ) );
		wp_die(
			'Global JS has been reset.',
			'Global JS Reset',
			array(
				'link_text' => 'Back to dashboard',
				'link_url'  => admin_url(),
			)
		);
	}
}
new BB_Global_JS_Reset;
